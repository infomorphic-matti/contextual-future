# contextual-future
This is a crate that provides an ergonomic means of *providing futures with lifetime-erased state sidechannel to `Future::poll`*.

## Why?
Currently, in rust, `Future` has a function `poll` that takes:
* A pinned exclusive/`mut` reference to self.
* A fixed reference to a fixed-type `&mut Context<'_>` 

This function then returns either `Poll::Pending` or `Poll::Ready(Future::Output)`.

In practise, there's not really a clean way to get valid, typed state out of this `Waker` API for modification - it's intended to provide a mechanism to wake the future up "later", but currently provides very few methods to obtain information from any type of runtime to do so. 

This means that current rust async runtimes - like `tokio`, `async-std`, and friends - end up having to use either global or thread-local variables to carry this state that their futures then attempt to access. 

In particular, this poses barriers to the use of async on embedded environments, despite the fact that rust futures are pretty much pure state machines and in theory match perfectly with environments that may lack good dynamic allocation or threading.

Furthermore, it often poses barriers to making runtime-agnostic libraries - in particular, it carries the issue of non-local dependency, where a wide-scale outer function has a dependency on global state deep in the call stack, rather than the state being provided through arguments or something similar. Whereas passing this state explicitly - along with some careful use of traits and transformations of contexts - should allow useful runtime agnosticism and granular dependency.

## Implementing `ContextHandle`
[`ContextHandle`] fundamentally represents some way to get hold of some form of shared storage with a shortened lifetime. This is what's stored inside the future. It's *conceptually speaking* a sort of `mut ptr`, from which lifetime-shortened references are generated as they are requested. In the easy case, it is certainly viable for this to just return simple *shared* references and manage synchronisation via the [`ContextHandle::Context`] as well. 

Certain types - like `Rc` - implicitly implement [`ContextHandle`] simply enough. They implicitly verify their lifetimes, by construction. In a single stack of `poll` calls on contextual futures or just plain futures, it should be impossible for more than one reference to be held to any internal context unless recursion of access occurs - in which case, a more sophisticated storage mechanism is desired.
