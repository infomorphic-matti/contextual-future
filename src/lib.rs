#![doc = include_str!("../README.md")]
#![no_std]
// Automatic feature docs.
#![cfg_attr(doc, feature(doc_auto_cfg))]
// #![feature(closure_lifetime_binder)]

use core::{pin::Pin, task::Poll};

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

/// The crate [`yoke`][yoke] fundamentally provides a way to manage types that reference things by
/// accessing it with a shorter lifetime in a safe manner - one which prevents, for instance, the
/// injection of shortened lifetimes into a given mutable reference to a shortened lifetime. 
///
/// The primary trait representing types that can be manipulated with shortened lifetimes is the
/// [`Yokeable`][yokeable] trait. 
///
/// However, this trait was developed before the stabilisation of GATs in Rust - meaning it takes a
/// lifetime parameter to the trait which is really a parameter for `Self` -  and it requires the
/// lifetime of whatever is being borrowed from to be `'static`. 
///
/// This is because, for instance, modifying a yoked type takes a function of lifetime `'static` - to 
/// avoid the injection of shorter references from within the function into the yoked value. However, 
/// there are many cases where some value may live for less than 'static - and where you might want
/// to inject some longer-lived references - but that may also want to be used with even shorter
/// lifetimes.
///
/// The [`Yokeable`][yokeable] trait also doesn't take advantage of the possibility of using the
/// [`Pin`][pin] system to allow for self-reference - though it does use the [`stable_deref_trait`][stablederef] 
/// crate. However, we desire to use this sort of lifetime erasure in `Future`s, which involves 
/// lots of Pin-Everything magic code - though luckily, the pin-y nature of this means that stable derefences 
/// are easier to ensure.
///
/// This module, then, takes advantage of several things. First, it takes advantage of GATs to
/// encode the shortened-lifetime value as a GAT without lowering it to a generic parameter of the
/// entire trait, simplifying placing bounds on it.
///
/// Then, it adds a lifetime parameter back, but this time to indicate the un-shortened lifetime of
/// the yokeable type. Certainly, anything that is `'static` can be used as normal - however,
/// structures that embed borrowed data but not from an already-`'static` source can parameterise
/// on their shorter lifetime.
///
/// [yoke]: https://docs.rs/yoke/latest/yoke/index.html
/// [yokeable]: https://docs.rs/yoke/latest/yoke/trait.Yokeable.html
/// [pin]: `core::pin::Pin`
/// [stablederef]: https://docs.rs/stable_deref_trait/latest/stable_deref_trait/index.html
pub mod yokeable {


}

pub mod ctx {
    use core::pin::Pin;

    /// Represents a lifetime-erased handle to some simple, shared memory.
    ///
    /// This extends the `yoke` crate's `Yokeable` concept with GATs and a non-static lifetime.
    /// It's not the same but it is similar in nature. The difference is that the method of
    /// accessing the handled location is implicitly part of the type rather than a separate trait.
    ///
    /// [See here][yoke]
    /// [yoke]: https://docs.rs/yoke/0.6.2/yoke/trait.Yokeable.html#why-is-this-safe
    pub trait SharedHandle<'primary>: 'primary {
        type SharedCtx<'a>: 'a + ?Sized
        where
            'primary: 'a;

        /// Perform an operation on the context.
        ///
        /// Function must be lifetime-universal to avoid injection into [`Self::SharedCtx`]
        ///
        /// Function must not contain any references to the shortened self lifetime, because that
        /// could be injected into self when in actuality self can live longer - this is the
        /// reason for the `'primary` bound.
        ///
        /// Unlike Yokeable, this does not prevent the binding-together of the SharedCtx lifetime parameter,
        /// and the lifetime of the passed reference to it. This is because self is actually pinned
        /// and we are comfortable using the notion of pinning to allow the risk of
        /// self-referentiality. Hence, no `for<'v>`.
        ///
        /// [See here][yoke]
        /// [yoke]: https://docs.rs/yoke/0.6.2/yoke/trait.Yokeable.html#why-is-this-safe
        fn do_with_shared<'s, R>(
            self: Pin<&'s mut Self>,
            f: impl 'primary + FnOnce(Pin<&'s Self::SharedCtx<'s>>) -> R,
        ) -> R
        where
            'primary: 's;
    }

    /// Represents a lifetime-erased handle to some context that can be exclusively accessed.
    ///
    /// This extends the `yoke` crate's concept of a `Yokeable` type with a GAT and non-static
    /// overall type lifetime, and it bundles the method of accessing storage as part of the type.
    ///
    /// [See here][yoke]
    /// [yoke]: https://docs.rs/yoke/0.6.2/yoke/trait.Yokeable.html#why-is-this-safe
    pub trait ContextHandle<'primary>: 'primary {
        type Ctx<'a>: 'a
        where
            'primary: 'a;

        /// Perform an operation on the context
        ///
        /// Function must live for as long as the original to avoid injection of
        /// too-short lifetimes to [`Self::Ctx`]. see [`SharedHandle::do_with_shared`]
        /// for some of the reasons behind the lifetime constraints.
        ///
        /// Unlike Yokeable, though, we do NOT need to worry about
        /// self-insertion of references, because we pass `Pin<&mut>` rather than just converting
        /// to plain `&mut`. Hence the lack of `for<'some-other-lt>` on the FnOnce to prevent
        /// injection of self-reference as in `Yokeable`.
        ///
        /// This is likely to require transmutation of the magical inner mutable contents into a
        /// shortened lifetime. Adding the 'primary requirement allows the avoidance of injecting
        /// shorter references - other than by extracting them from the existing, pinned argument.
        ///
        /// [See here][yoke]
        /// [yoke]: https://docs.rs/yoke/0.6.2/yoke/trait.Yokeable.html#why-is-this-safe
        fn do_with_exclusive<'s, R>(
            self: Pin<&'s mut Self>,
            f: impl 'primary + FnOnce(Pin<&'s mut Self::Ctx<'s>>) -> R,
        ) -> R
        where
            'primary: 's;
    }

    /// Core implementations for the various context types.
    pub mod _impl_core_shared {
        use core::{
            ops::{Deref, DerefMut},
            pin::Pin,
        };

        use super::SharedHandle;

        /*
        // We can't do things generically on `Deref`/`DerefMut`.
        impl<'primary, Ref: DerefMut + 'primary> SharedHandle<'primary> for Ref
        where
            <Ref as Deref>::Target: SharedHandle<'primary> + 'primary,
        {
            type SharedCtx<'a> = <Ref::Target as SharedHandle<'primary>>::SharedCtx<'a>;

            fn do_with_shared<'s, R>(
                self: Pin<&'s mut Self>,
                outer_transform: impl 'primary + FnOnce(Pin<&'s Self::SharedCtx<'s>>) -> R,
            ) -> R {
                <Ref::Target as SharedHandle<'primary>>::do_with_shared(
                    /* Pin::new_unchecked guaruntees the memory will not be moved - this is just
                     * getting the memory as a &mut rather than with the outer pointer.
                     *
                     * Self is pinned, and therefore the reference to the target is pinned as well
                     * */
                    unsafe { self.map_unchecked_mut(DerefMut::deref_mut) },
                    |v: Pin<&'s <Ref::Target as SharedHandle<'primary>>::SharedCtx<'s>>| -> R {
                        outer_transform(v)
                    },
                )
            }
        }
        */
    }

    #[cfg(feature = "alloc")]
    pub mod __impl_alloc_shared {
        use core::{ops::Deref, pin::Pin};

        use alloc::rc::Rc;

        use super::SharedHandle;

        impl<'q, T: 'q + ?Sized> SharedHandle<'q> for Rc<T> {
            type SharedCtx<'a> = T where 'q: 'a;

            fn do_with_shared<'s, R>(
                self: Pin<&'s mut Self>,
                f: impl 'q + FnOnce(Pin<&'s Self::SharedCtx<'s>>) -> R,
            ) -> R
            where
                'q: 's,
            {
                f(
                    /* SAFETY: deref just translates from a smart ptr to bare ptr counterpart*/
                    unsafe { self.into_ref().map_unchecked(Deref::deref) },
                )
            }
        }
    }
}

/// Represents a context that erases the lifetime of it's contents for extraction by
/// [`ContextFuture`]s. In particular, this represents exclusive access to some arbitrary context.
/// This can be synchronised between threads, or even thread-unsafe.
///
/// In particular, exclusive access to this handle does not imply exclusive access to
/// [`ContextHandle::Context`] implicitly - Your implementation must ensure this itself by
/// [`ContextHandle::do_with_context`]. If this handle is something like a refcounted pointer or something, it
/// might imply this, but it *definitely does not have to*.
///
/// To enable optimisations, this does not enable you to directly get the context. Instead, it
/// allows you to perform any operation that updates the context.
///
///
pub trait ContextHandle {
    /// Data to retrieve when needing exclusive access to the context itself.
    ///
    /// Note that exclusive access to [`Self`] has no reason to imply exclusive access to the
    /// obtained context. It may be desirable to, for instance, provide a wider shared context but
    /// then lock it for thread safety at a more granular level.
    type Context<'a>
    where
        Self: 'a;

    /// Perform an operation that modifies the context.
    ///
    /// This operation cannot be context lifetime specific. This is because if it were so, it would
    /// allow injecting data of the lifetime of the reference to context, into the context. Because
    /// the lifetime of the context is erased, this is not allowed, as in some cases the context
    /// may be used beyond the extent of the current reference.
    fn do_with_context<R, WithContextFn: for<'ctx> FnOnce(&mut Self::Context<'ctx>) -> R>(
        self: Pin<&mut Self>,
        f: WithContextFn,
    ) -> R;
}

pub mod context_handle_impls;

/// Trait to enable the construction of futures that carry lifetime-erased, accessible state.
///
/// This does not intrinsically enable the use of by-value context - for that, see
/// [`ContextTransformFuture`].
///
/// It is *strongly preferred* to have structures generic over contexts, with only a criteria upon
/// [`ContextHandle::Context`] used to determine behaviour. This allows for people to use different
/// means of referencing contextual data depending on their needs (e.g. thread safety or not).
pub trait ContextFuture {
    /// The context handle in this future. This is probably a reference to some external value.
    type CtxHandle: ContextHandle;

    /// The final output of the future.
    type Output;

    /// The non-[`Self::CtxHandle`] internal state.
    type State<'s>
    where
        Self: 's;

    /// Get the context *handle* and the actual state from this future.
    ///
    /// This has the same usage criteria as [`core::future::Future::poll`]. This is, in essence, a form of
    /// pin projection - just like the `pin-project` crates might do.
    fn get_handle_and_state(
        self: Pin<&mut Self>,
    ) -> (Pin<Self::State<'_>>, Pin<&mut Self::CtxHandle>);

    /// Like [`core::future::Future::poll`], except with access to a typed context (as well as the standard
    /// [`core::task::Context`] that provides wakeability.
    fn poll<'s, 'ctx, 'core_ctx>(
        state: Pin<Self::State<'s>>,
        context: &mut <Self::CtxHandle as ContextHandle>::Context<'ctx>,
        waker_context: &mut core::task::Context<'core_ctx>,
    ) -> Poll<Self::Output>
    where
        Self: 's;
}

/// Module allowing the implementation of [`core::future::Future`] on [`ContextFuture`] implementors via a
/// convenience method.
///
/// Rust orphan rules do not allow
pub mod adapters {
    use core::{future::Future, pin::Pin, task::Poll};

    use crate::{ContextFuture, ContextHandle};

    #[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord)]
    /// Wrapper struct that allows generic implementation of [`core::future::Future`] on
    /// [`ContextFuture`]. When this is pinned, so is the inner field.
    pub struct CxFut<T: ?Sized>(pub T);

    impl<T: ?Sized> CxFut<T> {
        /// Get pinned inner from pinned outer
        #[inline(always)]
        pub fn pin_project(self: Pin<&mut Self>) -> Pin<&mut T> {
            // SAFETY:
            // This is the only access of the field via pin, and noone else can get raw mut from
            // Pin<&mut Self> without unsafe.
            unsafe { self.map_unchecked_mut(|s| &mut s.0) }
        }
    }

    impl<T> From<T> for CxFut<T> {
        #[inline(always)]
        fn from(value: T) -> Self {
            Self(value)
        }
    }

    /// Extension trait to provide methods that ease converting [`ContextFuture`] into a plain future.  
    pub trait ContextFutureExt: ContextFuture {
        /// make this a [`core::future::Future`]
        #[inline(always)]
        fn cx(self) -> CxFut<Self>
        where
            Self: Sized,
        {
            CxFut(self)
        }
    }

    impl<T: ContextFuture> ContextFutureExt for T {}

    impl<T: ContextFuture> Future for CxFut<T> {
        type Output = T::Output;

        #[inline]
        fn poll(self: Pin<&mut Self>, cx: &mut core::task::Context<'_>) -> Poll<Self::Output> {
            let this = self.pin_project();
            // Resolve the handle
            let (state, typed_ctx) = this.get_handle_and_state();
            typed_ctx.do_with_context(move |lt_variant_context| {
                // Perform the stateful poll
                T::poll(state, lt_variant_context, cx)
            })
        }
    }
}

/// Common imports for ease of use.
pub mod prelude {
    pub use super::adapters::ContextFutureExt as _;
}

// contextual-future
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
