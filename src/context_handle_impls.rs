//! Implementations of [`crate::ContextHandle`] for various alloc and stdlib types

pub mod core_min {
    use chain_trans::prelude::*;
    use core::{cell::Cell, pin::Pin};

    use crate::ContextHandle;

}

#[cfg(feature = "alloc")]
pub mod alloc_min {}

// contextual-future
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
